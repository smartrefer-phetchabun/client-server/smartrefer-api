import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { RerferServerModel } from '../../models/ket/refer_server'

const rerferServerModel = new RerferServerModel();
export default async function referResult(fastify: FastifyInstance) {

    // select
    fastify.get('/select/:hcode',  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        // const token = req.headers.authorization.split(' ')[1];
        const hcode:any = req.params.hcode
        try {
            let res_: any = await rerferServerModel.select(hcode);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
    })

    fastify.get('/selectPhr',  async (request: FastifyRequest, reply: FastifyReply) => {
      const req: any = request
      // const token = req.headers.authorization.split(' ')[1];
      const hcode:any = req.params.hcode
      try {
          let res_: any = await rerferServerModel.selectPhr();
          reply.send(res_);
        } catch (error) {
          reply.send({ ok: false, error: error });
        }
  })


  fastify.get('/selectReferboard',  async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request
    // const token = req.headers.authorization.split(' ')[1];
    const hcode:any = req.params.hcode
    try {
        let res_: any = await rerferServerModel.selectReferboard();
        reply.send(res_);
      } catch (error) {
        reply.send({ ok: false, error: error });
      }
})

fastify.get('/selectServer',  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  // const token = req.headers.authorization.split(' ')[1];
  const hcode:any = req.params.hcode
  try {
      let res_: any = await rerferServerModel.selectServer();
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

fastify.get('/selectUpload',  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  // const token = req.headers.authorization.split(' ')[1];
  const hcode:any = req.params.hcode
  try {
      let res_: any = await rerferServerModel.selectUpload();
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

}