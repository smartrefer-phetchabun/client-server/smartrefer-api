import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class AppointSlotModel {

    async list(token: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/appoint_slot/list`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async select_id(token: any, id: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/appoint_slot/select_id?id=${id}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async select_hcode(token: any, hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/appoint_slot/select_hcode?hcode=${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async insert(token: any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'POST',
                url: `${urlApi}/appoint_slot/insert`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                body: { rows: rows },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async update(token: any, hcode: any, clinic_code: any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'PUT',
                url: `${urlApi}/appoint_slot/update?hcode=${hcode}&clinic_code=${clinic_code}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                body: { rows: rows },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async delete(token: any, hcode: any, clinic_code: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'DELETE',
                url: `${urlApi}/appoint_slot/delete?hcode=${hcode}&clinic_code=${clinic_code}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    // 'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }
}