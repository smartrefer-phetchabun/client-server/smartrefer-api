import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class EvaluateModel {

  async select(token:any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/evaluate/select`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async select_eva_detail(token:any, refer_no:any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/evaluate/select_eva_detail/${refer_no}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async eva_detail(token:any, rows: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'POST',
        url: `${urlApi}/evaluate/insert_eva_detail`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        body: { rows: rows }
        ,
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async select_eva_count(token:any, sdate:any, edate:any, hcode:any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/evaluate/select_eva_count/${sdate}/${edate}/${hcode}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async select_eva_show(token:any, sdate:any, edate:any, hcode:any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/evaluate/select_eva_show/${sdate}/${edate}/${hcode}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async select_eva_show_referin(token:any, sdate:any, edate:any, hcode:any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/evaluate/select_eva_show_referin/${sdate}/${edate}/${hcode}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async delEva_detail(token:any, referno:any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'DELETE',
        url: `${urlApi}/evaluate/delete_eva_detail/${referno}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          // 'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async update_eva_detail(token:any, referno:any, rows: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'PUT',
        url: `${urlApi}/evaluate/update_eva_detail/${referno}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        body: { rows: rows }
        ,
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

}