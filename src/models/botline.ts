const request = require("request");
export class BotlineModel {
    botLine(message: any, token: any) {
        request({
            method: 'POST',
            uri: 'https://notify-api.line.me/api/notify',
            header: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            auth: {
                // bearer: 'n9XdwQop63zVEdTvOkz4tGUz90LbIosbjmQYXVSM6EN', //token
                bearer: token, //token test code
            },
            form: {
                message: message, //ข้อความที่จะส่ง
            },
        }, (err:any, httpResponse:any, body:any) => {
            if (err) {
                console.log(err)
            } else {
                console.log(body)
            }
        })
    }
}